-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Počítač: localhost
-- Vytvořeno: Stř 26. úno 2020, 12:09
-- Verze serveru: 8.0.13-4
-- Verze PHP: 7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `vQ77vknWKI`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `learning`
--

CREATE TABLE `learning` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Vypisuji data pro tabulku `learning`
--

INSERT INTO `learning` (`id`, `name`, `surname`, `phone`, `email`, `user_id`) VALUES
(1, 'Pavel', 'Brdlik', 123456789, 'pavel.je@buh.cz', 15),
(2, 'Jiří', 'Přemyslovský', 776536555, 'sha384cz@gmail.com', 2);

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `knowledge_type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `knowledge_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `phone`, `email`, `knowledge_type`, `knowledge_desc`, `price`, `username`, `password`) VALUES
(1, 'Michal', 'Erben', 732194319, 'michal.erben@mensa.cz', 'Programovani', 'Jsem cool programator!', 100000, 'MichalErben', 'ahoj'),
(2, 'Jiří', 'Přemyslovský', 123456789, 'premyji17@sps-prosek.cz', 'balení jointů', 'ubalím naprosto dokonalýho jointa za 1 minutu', 420, 'bryen', '$2y$10$xlsdBXsyPCBVhClENN9u5OlJsV6iwINzWoSNqs0cE7f.9u6DPHIc6'),
(3, 'Pavel', 'Brdlík', 123456789, 'test@test.cz', 'idk', 'idk', 999, 'abc', 'abc'),
(5, 'Pavel', 'Brdlík', 123456789, 'test@test.cz', 'idk', 'idk', 999, 'abaac', 'abc'),
(6, 'Pavel', 'Brdlík', 123456789, 'test@test.cz', 'idk', 'idk', 999, 'abca', 'abc'),
(7, 'Pavel', 'Brdlík', 123456789, 'test@test.cz', 'idk', 'ubalím naprosto dokonalýho jointa za 1 minutuubalím naprosto dokonalýho jointa za 1 minutuubalím naprosto dokonalýho jointa za 1 minutuubalím naprosto dokonalýho jointa za 1 minutu', 999, 'abcc', 'abc'),
(8, 'Pavel', 'Brdlík', 123456789, 'test@test.cz', 'idk', 'idk', 999, 'abac', 'abc'),
(9, 'Pavel', 'Brdlík', 123456789, 'test@test.cz', 'idk', 'idk', 999, 'abdc', 'abc'),
(10, 'Pavel', 'Brdlík', 123456789, 'test@test.cz', 'idk', 'idk', 999, 'abfc', 'abc'),
(11, 'Pavel', 'Brdlík', 123456789, 'test@test.cz', 'idk', 'idk', 999, 'abgc', 'abc'),
(12, 'Pavel', 'Brdlík', 123456789, 'test@test.cz', 'idk', 'idk', 999, 'abrc', 'abc'),
(13, 'Pavel', 'Brdlík', 123456789, 'test@test.cz', 'idk', 'idk', 999, 'abec', 'abc'),
(15, 'Michal', 'Erben', 123456789, 'michal.erben@gmail.com', 'Programator', 'jsem buh', 100000, 'Michal', '$2y$10$P8iyDGGM1FvznbnEAZ3rb.0H/bRYmHD3JFH/4eVwMl4GE/rXmFjg6');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `learning`
--
ALTER TABLE `learning`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Klíče pro tabulku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `learning`
--
ALTER TABLE `learning`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pro tabulku `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `learning`
--
ALTER TABLE `learning`
  ADD CONSTRAINT `learning_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
