<?php


namespace App\Presenters;

use App\Forms\LearningFormFactory;
use Nette;
use App\Model\UserManager;


class LearnPresenter extends BasePresenter
{
    /** @var UserManager */
    private $userManager;

	/** @var LearningFormFactory */
	private $learningFormFactory;

	public function __construct(UserManager $userManager, LearningFormFactory $learningFormFactory){
		parent::__construct();
        $this->userManager = $userManager;
		$this->learningFormFactory = $learningFormFactory;
	}

	protected function createComponentLearningForm(){
		$id = $this->getParameter("id");
		$form = $this->learningFormFactory->create($id);
		$form->onSuccess[] = function () use ($id){
			$this->redirect("Learn:contact", ["id" => $id]);
		};

		return $form;
	}

    public function renderDefault(int $page = 1){
        $teachersCount = $this->userManager->getTeachersCount();

        $paginator = new Nette\Utils\Paginator;
        $paginator->setItemCount($teachersCount);
        $paginator->setItemsPerPage(12);
        $paginator->setPage($page);

        $teachers = $this->userManager->findAllTeachers($paginator->getLength(), $paginator->getOffset());

        $this->template->teachers = $teachers;

        $this->template->paginator = $paginator;
    }

	public function renderShow($id){

	}

	public function renderContact($id){
		$this->template->contact = $this->userManager->getById($id);
	}
}