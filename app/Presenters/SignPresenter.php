<?php

namespace App\Presenters;

use App\Forms;
use Nette\Application\UI\Form;

final class SignPresenter extends BasePresenter{
	/** @var Forms\SignInFormFactory */
	private $signInFactory;

	/** @var Forms\SignUpFormFactory */
	private $signUpFactory;

	public function __construct(Forms\SignInFormFactory $signInFactory, Forms\SignUpFormFactory $signUpFactory){
		parent::__construct();
		$this->signInFactory = $signInFactory;
		$this->signUpFactory = $signUpFactory;
	}

	protected function createComponentSignInForm(){
		$form = $this->signInFactory->create();
		$form->onSuccess[] = function (){
			$this->redirect("Dashboard:default");
		};

		return $form;
	}

	protected function createComponentSignUpForm(){
		$form = $this->signUpFactory->create();
		$form->onSuccess[] = function (){
			$this->redirect("Dashboard:default");
		};

		return $form;
	}


	public function actionOut(){
		$this->getUser()->logout();
		$this->redirect("Homepage:default");
	}
}
