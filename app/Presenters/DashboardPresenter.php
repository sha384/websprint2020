<?php

namespace App\Presenters;

use Nette;
use App\Model\LearningManager;

class DashboardPresenter extends BasePresenter {

    /** @var LearningManager */
    private $learningManager;
    public function __construct(LearningManager $learningManager){
        $this->learningManager = $learningManager;
    }
    public function renderDefault(int $page = 1){
        $candidatesCOunt = $this->learningManager->getCandidatesCount();

        $paginator = new Nette\Utils\Paginator;
        $paginator->setItemCount($candidatesCOunt);
        $paginator->setItemsPerPage(12);
        $paginator->setPage($page);

        $candidates = $this->learningManager->findAllCandidates($this->user->getId(), $paginator->getLength(), $paginator->getOffset());

        $this->template->candidates = $candidates;

        $this->template->paginator = $paginator;
    }
}