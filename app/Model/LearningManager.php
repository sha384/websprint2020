<?php


namespace App\Model;

use Nette;

class LearningManager
{
    /** @var Nette\Database\Context */
    private $database;

    public function __construct(Nette\Database\Context $database){
        $this->database = $database;
    }

    public function addLearning($name, $surname, $email, $phone, $teacher_id){
        return $this->database->query("INSERT INTO learning(name, surname, email, phone, user_id) VALUES(?, ?, ?, ?, ?)", $name, $surname, $email, $phone, $teacher_id);
    }

    public function findAllCandidates(int $id, int $limit, int $offset){
        return $this->database->query('
			SELECT * FROM learning WHERE user_id = ?
			LIMIT ?
			OFFSET ?',
            $id,
            $limit, $offset
        );
    }

    public function getCandidatesCount(): int
    {
        return $this->database->fetchField('SELECT COUNT(*) FROM learning');
    }
}