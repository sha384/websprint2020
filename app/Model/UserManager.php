<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;

class UserManager implements Nette\Security\IAuthenticator {
	use Nette\SmartObject;

	private const
		TABLE_NAME = 'users',
		ID = 'id',
		NAME = "name",
		SURNAME = "surname",
		USERNAME_COLUMN = 'username',
		PASSWORD_COLUMN = 'password',
		PHONE = "phone",
        EMAIL = "email",
       	KNOWLEDGE_TYPE = "knowledge_type",
		KNOWLEDGE_DESC = "knowledge_desc",
		PRICE = "price";


	/** @var Nette\Database\Context */
	private $database;

	/** @var Passwords */
	private $passwords;


	public function __construct(Nette\Database\Context $database, Passwords $passwords){
		$this->database = $database;
		$this->passwords = $passwords;
	}

	public function authenticate(array $credentials): Nette\Security\IIdentity{
		[$username, $password] = $credentials;

		$row = $this->database->table(self::TABLE_NAME)
			->where(self::USERNAME_COLUMN, $username)
			->fetch();

		if (!$row) {
			throw new Nette\Security\AuthenticationException('Uživatelské jméno nebo heslo je špatně.', self::IDENTITY_NOT_FOUND);
		} elseif (!$this->passwords->verify($password, $row[self::PASSWORD_COLUMN])) {
			throw new Nette\Security\AuthenticationException('Uživatelské jméno nebo heslo je špatně.', self::INVALID_CREDENTIAL);

		} elseif ($this->passwords->needsRehash($row[self::PASSWORD_COLUMN])) {
			$row->update([
				self::PASSWORD_COLUMN => $this->passwords->hash($password),
			]);
		}

		$arr = $row->toArray();
		unset($arr[self::PASSWORD_COLUMN]);
		return new Nette\Security\Identity($row[self::ID], null, $arr);
	}

	public function add(string $name, string $surname, string $username, string $password, int $phone, string $email, string $knowledge_type, string $knowledge_desc, int $price): void{
		try {
		    $data = [
				self::NAME => $name,
				self::SURNAME => $surname,
				self::USERNAME_COLUMN => $username,
				self::PASSWORD_COLUMN => $this->passwords->hash($password),
				self::PHONE => $phone,
				self::EMAIL => $email,
				self::KNOWLEDGE_TYPE => $knowledge_type,
				self::KNOWLEDGE_DESC => $knowledge_desc,
				self::PRICE => $price
			];

			$this->database->table(self::TABLE_NAME)->insert($data);
		} catch (Nette\Database\UniqueConstraintViolationException $e) {
			throw new DuplicateNameException;
		}
	}

    public function edit($id, $data){
	    if (!empty($data["password"])){
	        $data["password"] = $this->passwords->hash($data["password"]);
        }

        return $this->database->table(self::TABLE_NAME)
            ->where(self::ID, $id)
            ->update($data);
	}

    public function getAll(){
        return $this->database->table(self::TABLE_NAME)
            ->fetchAll();
	}

    public function getById($id){
        return $this->database->table(self::TABLE_NAME)
            ->where(self::ID, $id)
            ->fetch();
	}
	public function findAllTeachers(int $limit, int $offset){
        return $this->database->query('
			SELECT * FROM users
			LIMIT ?
			OFFSET ?',
            $limit, $offset
        );
    }

    public function getTeachersCount(): int
    {
        return $this->database->fetchField('SELECT COUNT(*) FROM users');
    }
}