<?php

declare(strict_types=1);

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;


final class FormFactory
{
	use Nette\SmartObject;

	public static function create($label = true): Form
	{
		$form = new Form;
		$form->onRender[] = function () use ($form, $label){
			$renderer = $form->getRenderer();
			$renderer->wrappers['controls']['container'] = null;
			$renderer->wrappers['pair']['container'] = 'div class="form-group row"';
			$renderer->wrappers['pair']['.error'] = 'has-danger';
			$renderer->wrappers['control']['container'] = $label ? 'div class=col-sm-9' : "div class=col-sm-12";
            $renderer->wrappers['label']['container'] = $label ? 'div class="col-sm-3 col-form-label"' : "";
            $renderer->wrappers['control']['description'] = 'span class=form-text';
			$renderer->wrappers['control']['errorcontainer'] = 'span class=invalid-feedback';
			$renderer->wrappers['control']['.error'] = 'is-invalid';
			foreach ($form->getControls() as $control) {
				$type = $control->getOption('type');
				if ($type === 'button') {
					$control->getControlPrototype()->addClass(empty($usedPrimary) ? 'btn btn-primary' : 'btn btn-secondary');
					$usedPrimary = true;
				} elseif (in_array($type, ['text', 'textarea', 'select'], true)) {
					$control->getControlPrototype()->addClass('form-control');
				} elseif ($type === 'file') {
					$control->getControlPrototype()->addClass('form-control-file');
				} elseif (in_array($type, ['checkbox', 'radio'], true)) {
					if ($control instanceof Nette\Forms\Controls\Checkbox) {
						$control->getLabelPrototype()->addClass('form-check-label');
					} else {
						$control->getItemLabelPrototype()->addClass('form-check-label');
					}
					$control->getControlPrototype()->addClass('form-check-input');
					$control->getSeparatorPrototype()->setName('div')->addClass('form-check form-check-inline');
				}
			}
		};
		return $form;
	}
}
