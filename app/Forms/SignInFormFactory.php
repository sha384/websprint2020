<?php

declare(strict_types=1);

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;


final class SignInFormFactory{
	/** @var User */
	private $user;


	public function __construct(User $user){
		$this->user = $user;
	}

	public function create(): Form {
		$form = FormFactory::create();

		$form->addText("username", "Uživatelské jméno")->setRequired();

		$form->addPassword("password", "Heslo")->setRequired();

		$form->addCheckbox("remember", "Zapamatovat si mě");

		$form->addSubmit("send", "Přihlásit se")->setHtmlAttribute("class", "myButton");

		$form->onSuccess[] = function (Form $form, $values){
			try {
				$this->user->setExpiration($values->remember ? '14 days' : '20 minutes');
				$this->user->login($values->username, $values->password);
			} catch (Nette\Security\AuthenticationException $e) {
				$form->addError('Uživatelské jméno nebo heslo je špatně.');
				return;
			}
		};

		return $form;
	}
}
