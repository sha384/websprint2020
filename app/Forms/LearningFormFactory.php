<?php

namespace App\Forms;

use App\Model\LearningManager;
use App\Model\UserManager;
use Latte\Engine;
use Nette;
use Nette\Application\UI\Form;

class LearningFormFactory{
	/** @var LearningManager */
	private $learningManager;

	/** @var UserManager */
	private $userManager;

	public function __construct(LearningManager $learningManager, UserManager $userManager){
		$this->learningManager = $learningManager;
		$this->userManager = $userManager;
	}

	public function create($id){
		$form = FormFactory::create();

		$form->addText("name", "Jméno")->setRequired();

		$form->addText("surname", "Příjmení")->setRequired();

		$form->addInteger("phone", "Telefonní číslo")
			->setRequired()
			->addCondition(Form::FILLED)
			->addRule(Form::LENGTH, "Telefonní číslo musí mít 9 číslic.", 9);

		$form->addEmail("email", "E-mail")->setRequired();

		$form->addSubmit("submit", "Zapsat se")->setHtmlAttribute("class", "myButton");

		$form->onSuccess[] = function (Form $form, $values) use ($id){
			$this->learningManager->addLearning(
				$values["name"],
				$values["surname"],
				$values["email"],
				$values["phone"],
				$id
			);

			$user = $this->userManager->getById($id);

			$mailer = new Nette\Mail\SmtpMailer([
				"host" => "smtp.sendgrid.net",
				"username" => "apikey",
				"password" => "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",
				"secure" => "ssl"
			]);

			$latte = new Engine();

			$message = new Nette\Mail\Message;
			$message->addTo($user->email);
			$message->setFrom("naucme@naucme.cz");
			$message->setSubject("Nový zájemce");
			$message->setHtmlBody($latte->renderToString(__DIR__ . "/email.latte", [
				"name" => $values["name"],
				"surname" => $values["surname"],
				"email" => $values["email"],
				"phone" => $values["phone"]
			]));

			$mailer->send($message);
		};

		return $form;
	}
}