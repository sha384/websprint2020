<?php

namespace App\Forms;

use App\Model;
use Nette;
use Nette\Application\UI\Form;

final class SignUpFormFactory{
	/** @var Model\UserManager */
	private $userManager;

	public function __construct(Model\UserManager $userManager){
		$this->userManager = $userManager;
	}

	public function create(): Form {
		$form = FormFactory::create();

		$form->addText("name", "Jméno")->setRequired();

		$form->addText("surname", "Příjmení")->setRequired();

		$form->addText("username", "Uživatelské jméno")->setRequired();

		$form->addPassword("password", "Heslo")->setRequired();

		$form->addInteger("phone", "Telefonní číslo")
			->setRequired()
			->addCondition(Form::FILLED)
			->addRule(Form::LENGTH, "Telefonní číslo musí mít 9 číslic.", 9);

		$form->addEmail("email", "E-mail")->setRequired();

		$form->addText("knowledge_type", "Druh znalosti")->setRequired();

		$form->addTextArea("knowledge_desc", "Popis znalosti", null, 5)->setRequired();

		$form->addInteger("price", "Cena za hodinu")->setRequired();

		$form->addSubmit("submit", "Zaregistrovat se")->setHtmlAttribute("class", "myButton");

		$form->onSuccess[] = function (Form $form, $values){
			try {
				$this->userManager->add(
					$values["name"],
					$values["surname"],
					$values["username"],
					$values["password"],
					$values["phone"],
					$values["email"],
					$values["knowledge_type"],
					$values["knowledge_desc"],
					$values["price"]
				);
			} catch (Model\DuplicateNameException $e) {
				$form['username']->addError('Toto jméno je již obsazené.');
				return;
			}
		};

		return $form;
	}
}
